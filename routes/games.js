const express = require('express')
const router = express.Router()
const path = require('path')

router.use(express.static(__dirname + '/../public'))

router.get('/rockpaperscissor' , (req,res) => {
    res.sendFile(path.join(__dirname + '/../public/game_suit.html'))
  })

module.exports = router
