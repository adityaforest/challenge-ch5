const express = require('express')
const app = express()
const port = 8000
const gamesRouter = require('./routes/games')
const fs = require('fs')

const user = require('./api/user')
app.use('/api/v1/users' , user)

app.use('/games', gamesRouter)
app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: true }))
app.use('/public', express.static('public'))

//VARS
//question: kalo dbstr & dbjson nya dibuat func sendiri return dbjson , .length nya jadi gabisa
let loggedInUserEmail = ""
let db = () =>{
  let dbStr = fs.readFileSync('./database.json').toString()
  let dbJSON = JSON.parse(dbStr)
  return dbJSON
}

let loggedInUserData = (targetEmail) => {
  
  let dbLen = db().userData.length
  for (let i = 0; i < dbLen; i++) {
    if (db()['userData'][i].email == targetEmail) {
      return {
        firstName: db()['userData'][i].firstName,
        lastName: db()['userData'][i].lastName,
        email: targetEmail,
        password: db()['userData'][i].password
      }
    }
  }
  return ""
}

//INDEX PAGE
app.get('/', (req, res) => {
  console.log(loggedInUserEmail)
  res.render('index', {
    user: loggedInUserData(loggedInUserEmail)
  })
})

app.post('/logout', (req, res) => {
  loggedInUserEmail = ""
  res.redirect('/')
})

//MY PROFILE PAGE
app.get('/myProfile', (req, res) => {
  res.render('myProfile', {
    error: false,
    message: "",
    user: loggedInUserData(loggedInUserEmail),
    emailInvalid: false,
  })
})

//question: kalo gak pake redirect gamau nyimpen kenapa ???
app.post('/backRedirTest', (req, res) => {
  res.redirect('/')
})

////question: kalo pake put jd ga works kenapa ???
app.post('/myProfile', (req, res) => {
  let { firstName, lastName, email, password } = req.body
  let allInputJSON = {
    "firstName": firstName,
    "lastName": lastName,
    "email": email,
    "password": password
  }

  let dbStr = fs.readFileSync('./database.json').toString()
  let dbJSON = JSON.parse(dbStr)
  let dbLen = dbJSON.userData.length

  for (let i = 0; i < dbLen; i++) {
    if (loggedInUserEmail != email) {
      if (dbJSON['userData'][i].email == email) {
        res.render("myProfile", {
          error: true,
          message: "Email address has been taken.",
          user: allInputJSON,
          emailInvalid: true,
        })
        return
      }
    }
  }

  for (let i = 0; i < dbLen; i++) {
    if (dbJSON['userData'][i].email == loggedInUserEmail) {
      dbJSON['userData'][i].firstName = firstName
      dbJSON['userData'][i].lastName = lastName
      dbJSON['userData'][i].email = email
      dbJSON['userData'][i].password = password
      break
    }
  }

  dbStr = JSON.stringify(dbJSON, null, 4)
  fs.writeFileSync('./database.json', dbStr)

  loggedInUserEmail = email
  res.status(200)
  res.render("myProfile", {
    error: false,
    message: "Your profile has been sucessfully updated.",
    user: loggedInUserData(loggedInUserEmail),
    emailInvalid: false,
  })
})

app.post('/deleteProfile', (req, res) => {
  let dbStr = fs.readFileSync('./database.json').toString()
  let dbJSON = JSON.parse(dbStr)
  let dbLen = dbJSON.userData.length

  for (let i = 0; i < dbLen; i++) {
    if (dbJSON['userData'][i].email == loggedInUserEmail) {
      dbJSON['userData'].splice(i,1)      
      break
    }
  }

  dbStr = JSON.stringify(dbJSON, null, 4)
  fs.writeFileSync('./database.json', dbStr)

  loggedInUserEmail = ""
  res.redirect('/')
})

//LOGIN PAGE
app.get('/login', (req, res) => {
  res.render('login', {
    message: "",
    form: {},
    emailInvalid: false,
    passwordInvalid: false
  })
})

app.post('/login', (req, res) => {
  //pick values from reqbody
  let { email, password } = req.body
  //create json for all input , used for refill form in redirected page if something is invalid
  let allInputJSON = {
    "email": email,
    "password": password
  }

  if (fs.existsSync('./database.json')) {
    //read and convert database file
    let dbStr = fs.readFileSync('./database.json').toString()
    let dbJSON = JSON.parse(dbStr)

    //check if email is exist in database and password match
    let dbLen = dbJSON.userData.length
    for (let i = 0; i < dbLen; i++) {
      if (dbJSON['userData'][i].email == email) {
        if (dbJSON['userData'][i].password == password) {
          loggedInUserEmail = email
          res.status(200)
          res.redirect("/")
          return
        }
        else {
          res.status(403)
          res.render("login", {
            message: "Wrong password. Try again or click Forgot password to reset it.",
            form: allInputJSON,
            emailInvalid: false,
            passwordInvalid: true
          })
          return
        }
      }
    }
  }
  res.status(403)
  res.render("login", {
    message: "Email is not registered. Please sign up first.",
    form: allInputJSON,
    emailInvalid: true,
    passwordInvalid: false
  })
})

///SIGNUP PAGE
app.get('/signup', (req, res) => {
  res.render('signup', {
    error: false,
    message: req.query.message,
    form: {},
    confPassInvalid: false,
    emailInvalid: false,
    agreementInvalid: false
  })
})

app.post('/signup', (req, res) => {
  //create new empty database.json if not exist
  if (!fs.existsSync('./database.json')) {
    let newDbJSON = { "userData": [{}] }
    fs.writeFileSync("./database.json", JSON.stringify(newDbJSON, null, 4))
  }
  //pick values from reqbody
  let { firstName, lastName, email, password, confirmPassword, agreementCheck } = req.body
  //question: yg dipush ke db userdatajson , tapi yg kepush allinputjson atau userdatajson yang udah ada tambahannya
  //create json for user data
  let userDataJSON = {
    "firstName": firstName,
    "lastName": lastName,
    "email": email,
    "password": password
  }
  //create json for all input , used for refill form in redirected page if something is invalid
  let allInputJSON = {...userDataJSON}
  allInputJSON.confirmPassword = confirmPassword
  allInputJSON.agreementCheck = agreementCheck ? true : false
  //read and convert database file
  let dbStr = fs.readFileSync('./database.json').toString()
  let dbJSON = JSON.parse(dbStr)

  //check if email already exist in database    
  let dbLen = dbJSON.userData.length
  for (let i = 0; i < dbLen; i++) {
    if (dbJSON['userData'][i].email == email) {
      //question: pake res.render soalnya untuk passing balik form inputnya biar user gak ngisi lagi , pake url?var=blalba bisa ga 
      res.render("signup", {
        error: true,
        message: "Email address has been taken.",
        form: allInputJSON,
        confPassInvalid: false,
        emailInvalid: true,
        agreementInvalid: false
      })
      return
    }
  }
  //check if confirmPassword and password not match
  if (confirmPassword != password) {
    res.render("signup", {
      error: true,
      message: "Password and Confirm Password must be match.",
      form: allInputJSON,
      confPassInvalid: true,
      emailInvalid: false,
      agreementInvalid: false
    })
    return
  }
  //check if agreement is not checked
  if (!agreementCheck) {
    res.render("signup", {
      error: true,
      message: "Please accept our Terms of Use & Privacy Policy",
      form: allInputJSON,
      confPassInvalid: false,
      emailInvalid: false,
      agreementInvalid: true
    })
    return
  }

  //push new data into database
  dbJSON['userData'].push(userDataJSON)
  //convert back and update database file
  dbStr = JSON.stringify(dbJSON, null, 4)
  fs.writeFileSync('./database.json', dbStr)
  res.status(200)
  res.render("signup", {
    error: false,
    message: "Signup success. Please login to your account.",
    form: {},
    confPassInvalid: false,
    emailInvalid: false,
    agreementInvalid: false
  })
})
//question: res.status penting ga sebenernya ? tanpa itu kan tetep work2 aja
//LISTEN ===========================================
app.listen(port, () => {
  console.log(`Express running on port ${port}`)
})