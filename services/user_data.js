const path = require('path')
const fs = require('fs')
const { user } = require('../api/user')
const dbDir = path.join(__dirname,'../database')
const dbFile = path.join(dbDir,'user.json')

const dataTemplate = {
    id : "",
    username : "",
    email : "",
    firstName : "",
    lastName : "",
    password : ""
}

const generateId = () => {
    return Date.now().toString(36) + Math.floor(Math.pow(10, 12) + Math.random() * 9*Math.pow(10, 12)).toString(36)
    // return Date.now().toString(36) + Math.random().toString(36).substring(2)
}

dbExist = () => {
    if (!fs.existsSync(dbDir)) {
        fs.mkdirSync(dbDir)
    }
}

getDb = () => {
    dbExist()
    if (!fs.existsSync(dbFile)) {
        fs.writeFileSync(dbFile, '[]')
    }
    const data = fs.readFileSync(dbFile)
    return JSON.parse(data.toString())
}

writeDb = (data) => {
    dbExist()
    fs.writeFileSync(dbFile, JSON.stringify(data, null, 2))
}

createUserData = (data) => {
    let users = getDb()
    data.id = generateId()
    users.unshift(data)
    writeDb(users)
}

readUserData = (prop,value) => {
    let users = getDb()
    let user = users.find(data => data[prop] == value)
    return user
}

updateUserData = (id, newData) => {
    let users = getDb()
    let newUsers = users.map(oldData => {
        if (oldData.id == id) {
            return Object.assign(oldData, newData)
        }
        return oldData
    })
    writeDb(newUsers)
}

deleteUserData = (id) => {
    let users = getDb()
    let newData = users.filter(oldData => oldData.id != id)
    writeDb(newData)
}

module.exports = {
    createUserData,
    readUserData,
    updateUserData,
    deleteUserData,
    getDb,
    dataTemplate
}