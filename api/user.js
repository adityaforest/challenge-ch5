const express = require('express')
const router = express.Router()
const userData = require('../services/user_data')

router.use(express.json())

//get all user data in db
router.get('/' , (req,res) => {    
    res.json(userData.getDb())
})

//get all user data in db by type/prop name and value
router.get('/:type/:value' , (req,res) => {
    let oldData= userData.readUserData(req.params.type , req.params.value)
    if(!oldData){
        res.status(404).json({
            message: "user data not found"
        })
        return
    }
    res.json(oldData)
})

//input new user data
router.post('/', (req,res) => {
    let newData = {...userData.dataTemplate} = req.body
    console.log(req.body)
    userData.createUserData(newData)

    res.status(201).json({
        message: "new user data created"
    })
})

router.put('/:id', (req,res) => {
    let oldData = userData.readUserData(req.params.id)
    if(!oldData){
        res.status(404).json({
            message:"user data not found"
        })
        return
    }

    let newData = {...userData.dataTemplate} = req.body
    let id = req.params.id

    userData.updateUserData(id , newData)

    res.status(201).json({
        message: "user data updated"
    })
})

router.delete('/:id', (req,res) => {
    let oldData = userData.readUserData(req.params.id)
    if(!oldData){
        res.status(404).json({
            message:"user data not found"
        })
        return
    }    

    let id = req.params.id

    userData.deleteUserData(id)

    res.status(201).json({
        message: "user data removed"
    })
})

module.exports = router